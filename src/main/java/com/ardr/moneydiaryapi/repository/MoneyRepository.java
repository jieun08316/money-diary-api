package com.ardr.moneydiaryapi.repository;

import com.ardr.moneydiaryapi.entity.Money;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoneyRepository extends JpaRepository<Money,Long> {

}
