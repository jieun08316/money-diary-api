package com.ardr.moneydiaryapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MoneyPayment {

    CARD_MONEY("카드"),
    REAL_MONEY("현금");

    private final String name;
}

