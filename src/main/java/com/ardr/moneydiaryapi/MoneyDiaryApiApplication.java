package com.ardr.moneydiaryapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MoneyDiaryApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MoneyDiaryApiApplication.class, args);
    }

}
