package com.ardr.moneydiaryapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class MoneyItem {

    private Long id;
    private LocalDate dateCreate;
    private String moneyContent;
    private Long price;
    private String moneyPayment;
    private String etcMemo;
    private String moneyEntryAndExit;

}
