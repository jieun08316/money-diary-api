package com.ardr.moneydiaryapi.model;


import com.ardr.moneydiaryapi.enums.MoneyEntryAndExit;
import com.ardr.moneydiaryapi.enums.MoneyPayment;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MoneyRequest {
    @Enumerated(value = EnumType.STRING)
    private MoneyEntryAndExit moneyEntryAndExit;

    @Enumerated(value = EnumType.STRING)
    private MoneyPayment moneyPayment;

    private String etcMemo;
    private String moneyContent;
    private Long price;
    }