package com.ardr.moneydiaryapi.controller;

import com.ardr.moneydiaryapi.model.MoneyItem;
import com.ardr.moneydiaryapi.model.MoneyRequest;
import com.ardr.moneydiaryapi.service.MoneyService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/money")

public class MoneyController {
private final MoneyService moneyService;

@PostMapping("/diary")
    public String setmoney(@RequestBody MoneyRequest request){
    moneyService.setMoney(request);
    return "OK";
}
    @GetMapping("/all")
    public List<MoneyItem> getMoneys(){
//        List<MoneyItem> result= moneyService.getMoneys();
        return moneyService.getMoneys();
    }

}