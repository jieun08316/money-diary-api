package com.ardr.moneydiaryapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MoneyEntryAndExit {

    EXIT_MONEY("지출"),
    ENTRY_MONEY("수입");

    private final String name;
}

