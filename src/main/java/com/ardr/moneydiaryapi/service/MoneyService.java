package com.ardr.moneydiaryapi.service;

import com.ardr.moneydiaryapi.entity.Money;
import com.ardr.moneydiaryapi.enums.MoneyPayment;
import com.ardr.moneydiaryapi.model.MoneyItem;
import com.ardr.moneydiaryapi.model.MoneyRequest;
import com.ardr.moneydiaryapi.repository.MoneyRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MoneyService {
    private final MoneyRepository moneyRepository;

    public void setMoney(MoneyRequest request) {
        Money addData = new Money();
        addData.setDateCreate(LocalDate.now());
        addData.setMoneyContent(request.getMoneyContent());
        addData.setPrice(request.getPrice());
        addData.setEtcMemo(request.getEtcMemo());
        addData.setMoneyPayment(request.getMoneyPayment());
        addData.setMoneyEntryAndExit(request.getMoneyEntryAndExit());
        moneyRepository.save(addData);
    }

    public List<MoneyItem> getMoneys() {
        List<Money> originList = moneyRepository.findAll();
        List<MoneyItem> result = new LinkedList<>();

        for(Money money : originList){ //향상된 for문
            MoneyItem addItem = new MoneyItem();
            addItem.setId(money.getId());
            addItem.setDateCreate(money.getDateCreate());
            addItem.setMoneyContent(money.getMoneyContent());
            addItem.setPrice(money.getPrice());
            addItem.setEtcMemo(money.getEtcMemo());
            addItem.setMoneyPayment(money.getMoneyPayment().getName());
            addItem.setMoneyEntryAndExit(money.getMoneyEntryAndExit().getName());
            result.add(addItem);
        }
        return result;
    }
}
