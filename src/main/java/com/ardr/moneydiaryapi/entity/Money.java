package com.ardr.moneydiaryapi.entity;

import com.ardr.moneydiaryapi.enums.MoneyEntryAndExit;
import com.ardr.moneydiaryapi.enums.MoneyPayment;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Money {
    @Id//시퀀스
    @GeneratedValue(strategy = GenerationType.IDENTITY) // 1씩증가
    private Long id;

    @Column(nullable = false)
    private LocalDate dateCreate;

    @Column(nullable = false, length = 40)
    private String moneyContent;

    @Column(nullable = false)
    private Long price;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;

    @Enumerated(value = EnumType.STRING)
    private MoneyPayment moneyPayment;

    @Enumerated(value = EnumType.STRING)
    private MoneyEntryAndExit moneyEntryAndExit;

}
